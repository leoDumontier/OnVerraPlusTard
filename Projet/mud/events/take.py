# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class TakeEvent(Event2):
    NAME = "take"

    def perform(self):
        if not self.object.has_prop("takable") or not self.actor.has_prop("peut_prendre_farine_boulanger_port") and self.object.has_prop("farine-boulanger-marche") or not self.actor.has_prop("peut_prendre_tenue_ouvrier") and self.object.has_prop("tenue-ouvrier"):
            self.add_prop("object-not-takable")
            return self.take_failed()

        for objName in self.object.name:
            if objName in self.actor.objectsNames:
                self.add_prop("object-already-in-inventory")
                return self.take_failed()

        for objName in self.object.names():
            if objName in self.actor.objectsNames:
                self.add_prop("object-already-in-inventory")
                return self.take_failed()

        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()

        self.object.move_to(self.actor)
        self.inform("take")

    def take_failed(self):
        self.fail()
        self.inform("take.failed")
