from .event import Event2
from random import randint

class UseEvent(Event2):
	NAME = "use"

	def perform(self):
		if not self.object.has_prop("useable"):
			self.fail()
			return self.inform("use.failed")

		if self.object.has_prop("randomusable"):
			if randint(0,3) != 1:
				self.fail()
				return self.inform("use.failed")
		self.inform("use")
