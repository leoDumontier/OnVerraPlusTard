from .event import Event3

class SayEvent(Event3):
	NAME = "say"

	def perform(self):
		if not self.object2.has_prop("responds") or not self.object.has_prop("sayable"):
			self.fail()
			return self.inform("say.failed")
		self.inform("say")