from .event import Event3

class GiveEvent(Event3):
	NAME = "give"

	def perform(self):
		if not self.object.has_prop("givable") or not self.object2.has_prop("thing-receiver") :
			self.fail()
			return self.inform("give.failed")
		if not self.object2.is_container():
			self.add_prop("object2-not-container")
			self.fail()
			return self.inform("give.failed")
		if self.object not in self.actor:
			self.add_prop("object-not-in-inventory")
			self.fail()
			return self.inform("give.failed")
		self.object.move_to(self.object2)
		self.inform("give")