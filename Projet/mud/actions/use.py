from .action import Action2
from mud.events import UseEvent

class UseAction(Action2):
	EVENT = UseEvent
	ACTION = "use"
	RESOLVE_OBJECT = "resolve_for_take"
