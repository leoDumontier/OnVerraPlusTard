from .action import Action3
from mud.events import SayEvent

class SayAction(Action3):
	EVENT = SayEvent
	ACTION = "say"
	RESOLVE_OBJECT = "resolve_for_take"
	RESOLVE_OBJECT2 = "resolve_for_operate"
