from .action import Action2
from mud.events import SpeakEvent

class SpeakAction(Action2):
	EVENT = SpeakEvent
	ACTION = "speak"
	RESOLVE_OBJECT = "resolve_for_operate"