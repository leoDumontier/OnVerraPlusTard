Configuration minimale requise :
  Python 3.x : https://www.python.org/downloads/
  Tornado : http://www.tornadoweb.org/en/stable/
  Pyyaml : http://pyyaml.org/
  Java : https://www.java.com/fr/

Comment jouer :
  - Aller dans le dossier 'Projet'
  - Lancer mud.py
  - Lancer son navigateur internet
  - Ouvrir l'url : http://localhost:9999
  - Vous pouvez aussi rejoindre un serveur sur le réseau local, ou sur internet