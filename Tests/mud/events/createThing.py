from .event import Event1
import mud
import os

class CreateThingEvent(Event1):
	NAME = "create-thing"

	def perform(self):
		mud.game.GAME.world.generatedObjects["badge"]=mud.game.GAME.world.generatedObjects.get("badge", 0)+1
		num=mud.game.GAME.world.generatedObjects["badge"]
		os.chdir("mud/games/projet")
		file=open("new_file.yml",'w')
		file.write("---" + os.linesep + "id: badge-"+str(num) + os.linesep + "type: Thing" + os.linesep + "name: badge" + os.linesep +"props:" + os.linesep +	"  - takable" + os.linesep +"  - key-for-porte-porche-000" + os.linesep +"events:" + os.linesep +"  info:" + os.linesep +'    actor: "un badge de sécurité"' + os.linesep +"  look:" + os.linesep +"    actor: |" + os.linesep +"      C'est un badge de sécurité permettant d'ouvrir des" + os.linesep +"      portes sécurisées.")
		file.close()
		os.chdir("../../../")
		mud.game.GAME.world.loadNewThing([mud.game.GAME.yaml_load("mud/games/projet/new_file.yml")])
		mud.game.GAME.world.database["badge-"+str(num)].move_to(self.actor)
		self.inform("create-thing")