from .action import Action1
from mud.events import CreateThingEvent

class CreateThingAction(Action1):
	EVENT = CreateThingEvent
	ACTION = "create-thing"